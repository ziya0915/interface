package Interfacemethods;

/**
 * this is the implementation of the class abstarct rectangle which is of the
 * abstract type
 * @author tashaik
 */
abstract class AbstractRectangle {
	public int length;
	int breadth;
	int height;
	int area;
	public int side;


	abstract void area();

	abstract void perimeter();

	abstract void volume();
}

class Rectangle extends AbstractRectangle {
	/*
	 * this is the helper method to find the area of the rectangle
	 */

	void area() {
		area = length * breadth;
		System.out.println("Area of the rectangle is : " + area);
	}
	/*
	 * this is the helper method to find the perimeter of the rectangle
	 */

	void perimeter() {
		int perimeter;
		perimeter = 2 * (length + breadth);
		System.out.println("Perimeter of the rectangle is : " + perimeter);
	}

	/*
	 * this is the helper method to find the volume of the rectangle
	 */
	void volume() {
		int volume;
		volume = length * breadth * height;
		System.out.println("Volume of the rectangle is : " + volume);

	}
}
