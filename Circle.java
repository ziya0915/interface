package Interfacemethods;

/**
 * this is the implementation of the class circle which 
 * inherits the methods of the clss shape
 * @author tashaik
 *
 */

public class Circle implements Shape {
	float pi = 22 / 7;
	int radius;
	
	/*
	 * this is the helper method 
	 * to find the area of circle
	 */

	public void area() {
		int area;
		area = (int) (pi * radius * radius);
		System.out.println("Area of circle is : " + area);
	}
	
	/*
	 * this is the helper method to
	 *  find the perimeter of the circle
	 */

	public void perimeter() {
		int perimeter;
		perimeter = (int) (2 * pi * radius);
		System.out.println("Perimeter of circle is : " + perimeter);
	}
	
	/*
	 * this is the helper method to
	 *  find the diameter of the circle
	 */
	
	public void diameter() {
        int diameter;
        diameter = 2 * radius;
        System.out.println("Diameter of the circle is : " + diameter);
	}
	
	

}
