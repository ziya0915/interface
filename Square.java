package Interfacemethods;

/**
 * this is the implementation of the class square which inherit the class
 * Abstract rectangle
 * @author tashaik
 */

public class Square extends AbstractRectangle {
	

	@Override
	/*
	 * this is the helper method to find the area of the square
	 */

	void area() {
		int area;
		area = side * side;
		System.out.println("Area of Square is : " + area);
	}

	@Override
	/*
	 * this is the helper method to find the perimeter of the square
	 */
	void perimeter() {
		int perimeter;
		perimeter = 4 * side;
		System.out.println("Perimeter of the square : " + perimeter);
	}

	@Override
	/*
	 * this is the helper method to find the volume of the square
	 */
	void volume() {
		int volume;
		volume = side * side * side;
		System.out.println("Volume of square is : " + volume);
	}
}
