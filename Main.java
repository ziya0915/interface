package Interfacemethods;

import java.util.Scanner;

/**
 * this is the implementation of the class which performs all the operations of
 * circle and rectangle
 * 
 * @author tashaik
 */

public class Main {
	@SuppressWarnings("resource")
	public static void main(String args[]) {
        int ch;
		Scanner sc = new Scanner(System.in);
		do {
		System.out.println("1. Operations of Circle : ");
		System.out.println("2. Operations of Rectangle : ");
		System.out.println("3. Operations of Square : ");
		System.out.println("Enter your choice :");
		 ch = sc.nextInt();

		switch (ch) {
		
		/**
		 * creating the object of the class Circle 
		 * to call all the methods
		 */
		case 1:
			Circle cir = new Circle();
			System.out.println("Enter the radius of the circle : ");
			int radius = sc.nextInt();
			cir.radius = radius;
			cir.area();
			cir.perimeter();
			cir.diameter();
			break;

			/**
			 * creating the object of the class Rectangle 
			 * to call all the methods
			 */
		case 2:
			AbstractRectangle rect = new Rectangle();
			System.out.println("Enter the length of the rectangle : ");
			int length = sc.nextInt();
			rect.length = length;
			System.out.println("Enter the Breadth of the rectangle : ");
			int breadth = sc.nextInt();
			rect.breadth = breadth;
			System.out.println("Enter the Height  of the rectangle : ");
			int height = sc.nextInt();
			rect.height = height;
			rect.area();
			rect.perimeter();
			rect.volume();
			break;

			/**
			 * creating the object of the class Square 
			 * to call all the methods
			 */
			case 3:
			AbstractRectangle sq = new Square();
			System.out.println("Enter the Side of the Square : ");
			int side = sc.nextInt();
			sq.side = side;
			sq.area();
			sq.perimeter();
			sq.volume();
			break;
			
			default :
				System.out.println("invalid choice...");

		}

	}while(ch!=0);
	}
}
