package Interfacemethods;

 interface Shape {
	 void area();
	 void perimeter();
	 void diameter();
}
